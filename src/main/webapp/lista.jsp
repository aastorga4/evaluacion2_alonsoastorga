<%-- 
    Document   : lista
    Created on : 25-04-2021, 21:53:06
    Author     : Alonso
--%>

<%@page import="root.entity.Jugadores"%>
<%@page import="java.util.Iterator"%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Jugadores> usuarios = (List<Jugadores>) request.getAttribute("listaJugadores");
    Iterator<Jugadores> itJugadores = usuarios.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form  name="form" action="BaseController" method="POST">
           <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
            
               
               <table border="1">
                    <thead>
                    <th>id</th>
                    <th>nickname </th>
             
                    <th> nivel</th>
                    <th> horas</th>
                    <th> premium</th>
                    </thead>
                    <tbody>
                        <%while (itJugadores.hasNext()) {
                      Jugadores jug = itJugadores.next();%>
                        <tr>
                            <td><%= jug.getId()%></td>
                            <td><%= jug.getNickname()%></td>
                          <td><%= jug.getNivel()%></td>
                          <td><%= jug.getTotalHoras()%></td>
                          <td><%= jug.getPremium ()%></td>
                 <td> <input type="radio" name="seleccion" value="<%= jug.getId()%>"> </td>
                
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
             <button type="submit" name="accion" value="ver" class="btn btn-success">ver jugador</button>
            <button type="submit" name="accion" value="eliminar" class="btn btn-success">eliminar</button>
            <button type="submit" name="accion" value="editar" class="btn btn-success">editar</button>


        
        </form>
    </body>
</html>

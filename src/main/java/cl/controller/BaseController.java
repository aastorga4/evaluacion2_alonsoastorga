/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller;

import cl.dao.JugadoresJpaController;
import cl.dao.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.entity.Jugadores;

/**
 *
 * @author Alonso
 */
@WebServlet(name = "BaseController", urlPatterns = {"/BaseController"})
public class BaseController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BaseController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BaseController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("doPost");

        String botonSeleccionado = request.getParameter("accion");
        //JugadoresJpaController dao= new JugadoresJpaController();
        System.out.println("el boton selecciona es:" + botonSeleccionado);
        JugadoresJpaController dao = new JugadoresJpaController();
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

        List<Jugadores> listaJugadores = null;

        EntityManager em = emf.createEntityManager();

        if (botonSeleccionado.equals("list")) {

            listaJugadores = dao.findJugadoresEntities();

            request.setAttribute("listaJugadores", listaJugadores);
            request.getRequestDispatcher("lista.jsp").forward(request, response);

        }
         if (botonSeleccionado.equals("ingreso")) {

            request.getRequestDispatcher("registrar.jsp").forward(request, response);

        }
          if (botonSeleccionado.equals("nuevoJugador")) {

            String id = request.getParameter("id");
            String nickname = request.getParameter("nickname");
            String nivel = request.getParameter("nivel");
            String horas = request.getParameter("total_horas");
            String premium = request.getParameter("premium");
            
            Jugadores jugador=new Jugadores();
            jugador.setId(id);
           jugador.setNickname(nickname);
            jugador.setNivel(nivel);
            jugador.setTotalHoras(horas);
            jugador.setPremium(premium);
            
            try {
                dao.create(jugador);
            } catch (Exception ex) {
                Logger.getLogger(BaseController.class.getName()).log(Level.SEVERE, null, ex);
            }
            listaJugadores = dao.findJugadoresEntities();

            request.setAttribute("listaJugadores", listaJugadores);
            request.getRequestDispatcher("lista.jsp").forward(request, response);
        }
          if (botonSeleccionado.equals("eliminar")) {
             
              String elementoSeleccionado = request.getParameter("seleccion");
              System.out.println("elementoSeleccionado"+elementoSeleccionado);
              
            try {
                dao.destroy(elementoSeleccionado);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(BaseController.class.getName()).log(Level.SEVERE, null, ex);
            }
              listaJugadores = dao.findJugadoresEntities();

            request.setAttribute("listaJugadores", listaJugadores);
            request.getRequestDispatcher("lista.jsp").forward(request, response); 
              
         }
          if(botonSeleccionado.equals("editar")){
              String id= request.getParameter("seleccion");
              Jugadores jugador= dao.findJugadores(id);
              request.setAttribute("jugador", jugador);
              request.getRequestDispatcher("editar.jsp").forward(request, response); 
              
             
          }
            if(botonSeleccionado.equals("edit")){
            String id = request.getParameter("id");
            String nickname = request.getParameter("nickname");
            String nivel = request.getParameter("nivel");
            String horas = request.getParameter("total_horas");
            String premium = request.getParameter("premium");
            
            Jugadores jugador = dao.findJugadores(id);
           // jugador.setNickname(nickname);
           // jugador.setNivel(nivel);
            //jugador.setTotalHoras(horas);
            //jugador.setTotalHoras(premium);
                    
                  try {
                      dao.edit(jugador) ;
                  } catch (Exception ex) {
                      Logger.getLogger(BaseController.class.getName()).log(Level.SEVERE, null, ex);
                  }
              listaJugadores = dao.findJugadoresEntities();

            request.setAttribute("listaJugadores", listaJugadores);
            request.getRequestDispatcher("lista.jsp").forward(request, response); 
              }
        

        /*try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Jugadores.class));
            Query q = em.createQuery(cq);
            
             listaJugadores= q.getResultList();
             
             System.out.println("la cantidad de jugadores en BD es:"+listaJugadores.size());
            
        } finally {
            em.close();
        }*/
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
